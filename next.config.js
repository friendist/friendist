const withCss = require('@zeit/next-css');
const withImages = require('next-images')

module.exports = withImages(withCss({
  
  devIndicators: {
    autoPrerender: false,
  },
  compress: false,
  target: 'serverless',
  webpack: (config, { isServer }) => {
    if (isServer) {
      const antStyles = /antd\/.*?\/style\/css.*?/;
      const origExternals = [...config.externals];
      config.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback();
          if (typeof origExternals[0] === 'function') {
            origExternals[0](context, request, callback);
          } else {
            callback();
          }
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals),
      ];

      config.module.rules.unshift({
        test: antStyles,
        use: 'null-loader',
      });
    }
    if (!isServer) {
      config.node = {
        fs: 'empty',
        net:'empty',
        tls:'empty'
      }
    }
    config.optimization.minimize = false;
    return config;
  },
  
}));
