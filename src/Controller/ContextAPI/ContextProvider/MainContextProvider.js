import {createContext, useReducer,useContext} from 'react';
import {ShowMenuInt_State} from '../../../utils/InitialState'

import MainReducer from '../Reducer/MainReducer'

 export const MainContext=createContext();
 

function MainContextProvider(props)
{
  const [ShowMenu, dispatch]=useReducer(MainReducer,ShowMenuInt_State)

    return(
<MainContext.Provider value={{ShowMenu, dispatch}}>
    {props.children}
</MainContext.Provider>
    )
}
export default MainContextProvider;

