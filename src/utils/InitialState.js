
export const ShowMenuInt_State={
 showmenu:false
};
export const LineChartData = [
    {
      name: "2008",
      Pro1: 483,
      Pro2: 236,
      Pro3: 300
    },
    {
      name: "2009",
      Pro1: 341,
      Pro2: 380,
      Pro3: 370
    },
    {
      name: "2010",
      Pro1: 480,
      Pro2: 270,
      Pro3: 326
    },
    {
      name: "2011",
      Pro1: 370,
      Pro2: 354,
      Pro3: 320
    },
    {
      name: "2012",
      Pro1: 450,
      Pro2: 337,
      Pro3: 250
    },
    {
      name: "2013",
      Pro1: 420,
      Pro2: 375,
      Pro3: 540
    },
    {
      name: "2014",
      Pro1: 270,
      Pro2: 316,
      Pro3: 410
    },
    {
      name: "2015",
      Pro1: 350,
      Pro2: 230,
      Pro3: 400
    }
];