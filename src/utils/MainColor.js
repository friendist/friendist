export const MainColor={
    WhiteGray:'#f2f2f2',
    Grey:'#9D9CA1',
    Cyan:'#0FAFE7',
    Blue:'#287DF9',
    Purple:'#AE61D4',
    Green:'#4FCF53',
    Orange:'#FFB40C',
    Red:'#F8493D',
    White:'#FFF',
    Black:'#000',
    Transparent:'transparent'
}
export const TextColor={
    Grey:'#595959',
    
}