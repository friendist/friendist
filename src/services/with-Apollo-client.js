import React from 'react';
import { ApolloProvider  } from '@apollo/react-hooks';
import {getDataFromTree } from "react-apollo";

 //import ssrPrepass from 'react-ssr-prepass'
import initApolloClient from './init-Apollo-client';
const withApolloClient = App =>{
    const withApollo=props=>{
        const apolloClient=React.useMemo(
            ()=>props.apolloClient || initApolloClient(props.apolloState)[0],[]
        );
        return(
            <ApolloProvider client={apolloClient}>
                <App {...props} apolloClient={apolloClient}/>
            </ApolloProvider>
        );
    };
    withApollo.getInitialProps = async ctx=>{
        const {AppTree} = ctx;

        let appProps ={};
        if(App.getInitialProps) 
        appProps = await App.getInitialProps(ctx);

        const isBrowser = typeof window !== 'undefined';
        if (isBrowser) return appProps;


        await getDataFromTree(
            <AppTree
              pageProps={{
                ...appProps,
               
              }}
            />
          );

      

          return {
            ...appProps,
          };
        };
      
        return withApollo;
      };
      export default withApolloClient;
