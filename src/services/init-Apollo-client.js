import ApolloClient from "apollo-client";
import { WebSocketLink } from 'apollo-link-ws';
import { createHttpLink } from 'apollo-link-http';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
let apolloClient = null;
export default function initApolloClient(initialState) {
  const httpLink = new createHttpLink({
    uri: "https://gostartup-hasura.herokuapp.com/v1/graphql",
    credentials: 'same-origin'
  });
  const wsLink = process.browser ? new WebSocketLink({ // if you instantiate in the server, the error will be thrown
    uri: `wss://gostartup-hasura.herokuapp.com/v1/graphql`,
    options: {
      lazy: true,
            reconnect: true,
            connectionParams: () => {
                return {headers:{'x-hasura-admin-secret': '69ed166662145f00edd487985428ad93'}}
            }
    }
  }) : null;
  const link = process.browser ? split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query);
      return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink,
  ): httpLink;
  const authLink = setContext((_, { headers }) => {
    const token = (typeof (localStorage) !== 'undefined' && localStorage != null) ? localStorage.getItem('token') : ''
    return {
      headers: {
        'x-hasura-admin-secret': '69ed166662145f00edd487985428ad93'
      }
    }
  });
  const isServer = typeof window === 'undefined';
  if (isServer || !apolloClient) {
    apolloClient = new ApolloClient({
      link:authLink.concat(link),
      cache: new InMemoryCache(),
    });
    
  }

  return [apolloClient];

}