import React, { useContext } from 'react';
import { Row, Col, Layout, Menu, Icon } from 'antd'
import HeaderPage from '../General/Header/MainHeader'
import MenuPage from '../General/Menu/MainMenu'
import { Helmet } from 'react-helmet'
import Head from 'next/head'
import { MainContext } from '../../Controller/ContextAPI/ContextProvider/MainContextProvider'
import { MainColor } from '../../utils/MainColor'
const { Header, Content, Sider, Footer } = Layout;
const SiderStyle = {
    overflow: 'auto',
    position: 'fixed',
    left: 0,
    height: '100%',
    zIndex: 1,
    boxShadow: `0px 0px 3px 0px ${MainColor.Grey}`
}
const HeaderStyle = {
    background: MainColor.White,
    padding: 0
}

const MainLayout = (props) => {
    const { ShowMenu } = useContext(MainContext);
    const { children } = props;
    return (
        <React.Fragment>
            <Helmet>
                <Head>
                    <title>Home</title>
                </Head>
            </Helmet>
            <Layout style={{ width: '100vw', height: '100vh' }}>
                <Header style={HeaderStyle}>
                    <HeaderPage />
                </Header>
                <Layout>
                    <Sider theme="light" trigger={null} width={250} collapsible collapsed={!ShowMenu.showmenu} collapsedWidth={80}
                        style={SiderStyle}>
                        <MenuPage />
                    </Sider>
                    <Content style={{ overflow: 'auto', margin: '2px 2px', padding: 24, background: MainColor.Transparent, marginLeft: !ShowMenu.showmenu ? 82 : 250 }}>
                        {children}
                    </Content>
                </Layout>
            </Layout>

            <style jsx global>
                {`
                /* width */
                ::-webkit-scrollbar {
                    width: 6px;
                    height: 8px;
                } 
                  /* Track */
                  ::-webkit-scrollbar-track {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1);
                    opcity:1;
                    background: #fff;
                    border-radius: 10px;
                  }
                  /* Handle */
                  ::-webkit-scrollbar-thumb {
                      
                    background: #888;
                    border-radius: 2px;
                  }
                  /* Handle on hover */
                  ::-webkit-scrollbar-thumb:hover {
                    background: #555; 
                    border-radius: 5px;
                  }
               
                `}
            </style>

        </React.Fragment>

    );

}

export default MainLayout;