import React from 'react';
import MainContextProvider from '../../Controller/ContextAPI/ContextProvider/MainContextProvider'

const FullLayout = (props) => {
    const { children } = props;
    return (
        <React.Fragment>
            <MainContextProvider>
            {children}
            </MainContextProvider>
        </React.Fragment>

    );

}

export default FullLayout;