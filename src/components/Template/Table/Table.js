import React from 'react'
import { Table} from 'antd';

const TableTemp = (props) => {
    const {HeaderList,DataSource}=props;
    return (
      <>
        <Table dataSource={DataSource} columns={HeaderList} pagination={false} />
      </>
    )
}
export default TableTemp;