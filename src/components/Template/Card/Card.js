import React from 'react'
import { Card, Avatar, Row, Typography, Col} from 'antd'
import { TextColor, MainColor } from '../../../utils/MainColor';
import AnimatedNumber from "animated-number-react";

const {Text} = Typography;
export default function CardTemp(props) {
    const {Title,icon,text,color}=props;
    
const formatValue = text => text.toFixed(0).replace(/\d(?=(\d{3}))/g, '$&,');

    return (

        <Card style={{border: `1px solid ${MainColor.WhiteGray}`,borderRadius:2}}>
        <Row type='flex' justify='start' align="middle" style={{width:'100%'}}> 
        <Avatar icon={icon} size={100} style={{color:color,background:MainColor.Transparent}}></Avatar>
        <Col span={12}>
        <Col span={24}>
        <Text style={{fontSize:18}}>{Title} </Text>
        </Col>
        <Col span={24}>
        <Text style={{fontSize:18,fontWeight:'bold',color:TextColor.Grey}}>
          
        <AnimatedNumber
          value={text}
          duration={text}
          easing='linear'
          formatValue={formatValue}
        />            </Text>
        </Col>
        </Col>
   
        </Row>
        </Card>
    )
}
