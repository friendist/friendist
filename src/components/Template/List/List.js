import React from 'react'
import { List, Avatar} from 'antd'
import CardTemp from './CardTemp'
import { MainColor } from '../../../utils/MainColor'
const ListTemp = (props) => {
    const data = [
        {
          title: 'Title 1',
          description:'Ant Design, a design language for background applications, is refined by Ant UED Team 1',
          Color:MainColor.Orange,
          text:'PENDENG',
          time:'2018-08-01 05:00'
        },
        {
          title: 'Title 2',
          description:'Ant Design, a design language for background applications, is refined by Ant UED Team 2',
          Color:MainColor.Red,
          text:'PREJECTED',
          time:'2017-12-08 12:40'
        },
        {
          title: 'Title 3',
          description:'Ant Design, a design language for background applications, is refined by Ant UED Team 3',
          Color:MainColor.Green,
          text:'FINISH',
          time:'2016-10-07 08:00'
        },
        
      ]
    return (
        <List
    itemLayout="horizontal"
    dataSource={data}
    renderItem={item => (
      <List.Item>
          <CardTemp Image={'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'} 
          TitleCard={item.title}
          Description={item.description}
          ButtonColor={item.Color}
          ButtonText={item.text}
          DateTime={item.time}
          />
    
      </List.Item>
    )}/>  
    )
}
export default ListTemp;