import React, { Component } from 'react'
import { Avatar, Row, Col,Typography, Button } from 'antd'
import { MainColor } from '../../../utils/MainColor';
const {Title,Text}=Typography;
const CardTemp=(props)=> {
 const {Image,TitleCard,Description,ButtonColor,ButtonText,DateTime}=props;
        return (
            <>
            <Row type='flex' justify='space-between' align='stretch' style={{width:'100%'}} gutter={[24,24]}>
                <Col  xs={4}  lg={3} xl={2}>
                <Avatar size={64} src={Image}/>
                </Col>
                <Col xs={20}  lg={20} xl={22} >
<Row type='flex' justify='start' align='stretch'>
<Col span={24}> <Title level={4}> {TitleCard}</Title></Col>
<Col span={24}>  <Text>{Description}</Text></Col>

<Col span={24}>
    <Row type='flex' justify='space-between' align='bottom' style={{marginTop:15}}>
  
   <Button  style={{background:ButtonColor,width:100,color:MainColor.White}}>{ButtonText}</Button>
   <Text>{DateTime}</Text>
    </Row>
</Col>
   
   
</Row>
                </Col>
            </Row>
              
            
            </>
          
        )
}
export default CardTemp;