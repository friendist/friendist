import React, { useContext } from 'react'
import { Menu, Icon, Row } from 'antd'
import { MainColor } from '../../../utils/MainColor';
import { MainContext } from '../../../Controller/ContextAPI/ContextProvider/MainContextProvider'

const { SubMenu } = Menu;
const MainMenu = () => {
    const { ShowMenu } = useContext(MainContext);

    return (
        <>

            <Menu style={{ marginBottom: 64 }} mode="inline" defaultSelectedKeys={['1']}>
                <Menu.Item key="1">
                    <Icon type="dashboard" />
                    <span>Dashboard</span>
                </Menu.Item>
                <Menu.Item key="2">
                    <Icon type="user" />
                    <span>Users</span>
                </Menu.Item>
                <Menu.Item key="3">
                    <Icon type="slack" />
                    <span>Todo</span>
                </Menu.Item>

                <SubMenu
                    key="sub1"
                    title={
                        <span>
                            <Icon type="rise" />
                            <span>Charts</span>
                        </span>
                    }>
                    <Menu.Item>
                        <Icon type="area-chart" />
                        <span>Area Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="bar-chart" />
                        <span>Bar Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="line-chart" />
                        <span>Line Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="pie-chart" />
                        <span>Pie Chart</span>
                    </Menu.Item>

                    <Menu.Item>
                        <Icon type="dot-chart" />
                        <span>Composed Chart</span>
                    </Menu.Item>

                    <Menu.Item>
                        <Icon type="radar-chart" />
                        <span>Radar Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="bar-chart" />
                        <span>Radial Bar Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="bar-chart" />
                        <span>Scatter Chart</span>
                    </Menu.Item>
                    <Menu.Item>
                        <Icon type="fall" />
                        <span>Tree map</span>
                    </Menu.Item>
                </SubMenu>




            </Menu>
        </>
    )
}
export default MainMenu;