import React, { useContext } from 'react'
import { Row, Icon, Form } from 'antd'
import { SHOWMENU } from '../../../Controller/ContextAPI/Actions/Main_Action'
import { MainContext } from '../../../Controller/ContextAPI/ContextProvider/MainContextProvider'
import { MainColor } from '../../../utils/MainColor'

const MainHeader = () => {
    const { ShowMenu, dispatch } = useContext(MainContext);

    function MenuAction() {
        dispatch(SHOWMENU());
    }

    return (
        <>
            <Row type='flex' style={{
                position: "fixed",
                zIndex: 2,
                background: MainColor.White,
                height: 64,
                width: '100%',
                boxShadow: `0px 0px 8px 1px ${MainColor.Grey}`,
            }}>
                <React.Fragment>
                    <Icon type="dingding" style={{ fontSize: 40, color: MainColor.Blue, paddingTop: 10,paddingLeft: 15}} />
                    <span hidden={!ShowMenu.showmenu} style={{ fontSize: 40, color: MainColor.Blue }}>Friendist</span>
                </React.Fragment>

                <Row style={{ background: MainColor.White, marginLeft: ShowMenu.showmenu ? 60 : 40}}>
                    <Icon
                        style={{ fontSize: 20 }}
                        className="trigger"
                        type={ShowMenu.showmenu ? 'menu-fold' : 'menu-unfold'}
                        onClick={MenuAction}
                    />
                </Row>

            </Row>
        </>
    )
}


export default MainHeader;