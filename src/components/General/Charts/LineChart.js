import React, { Component } from "react";
import { LineChartData } from "../../../utils/InitialState";
import { Row, Col } from 'antd';
import {
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,

  Line,
  ResponsiveContainer
} from "recharts";
import { MainColor } from "../../../utils/MainColor";
const Line_Chart = () => {

  return (
    <>
      <Row type='flex' style={{ background: MainColor.White, border: `1px solid ${MainColor.WhiteGray}`, borderRadius: 2 }}>
        <Col span={24}><h1 style={{ textAlign: "left", marginLeft: 100 }}>Line Chart</h1></Col>
        <ResponsiveContainer width='100%' height={430}>
          <LineChart
            width="100%"
            data={LineChartData}
            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis  />
            <Tooltip />
            <Legend layout="horizontal" verticalAlign="top" align="right" 
              payload={
                [
                  { id: 'Pro1', value: 'Pro1', type: 'circle', color: 'rgb(216, 151, 235)' },
                  { id: 'Pro2', value: 'Pro2', type: 'circle', color: 'rgb(246, 152, 153)' },
                  { id: 'Pro3', value: 'Pro3', type: 'circle', color: 'rgb(100, 234, 145)' },
                ]
              }
              wrapperStyle={{
                paddingBottom: "30px"
            }}
              />
            <Line type="monotone" dataKey="Pro1" stroke="rgb(216, 151, 235)" />
            <Line type="monotone" dataKey="Pro2" stroke="rgb(246, 152, 153)" />
            <Line type="monotone" dataKey="Pro3" stroke="rgb(100, 234, 145)" />
          </LineChart>
        </ResponsiveContainer>
      </Row>
    </>
  );

}
export default Line_Chart;