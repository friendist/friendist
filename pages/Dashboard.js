import React from 'react'
import Head from 'next/head'
import MainLayout from '../src/components/Layout/MainLayout'
import { Row, Col,Typography,Tag  } from 'antd'
import MainContextProvider from '../src/Controller/ContextAPI/ContextProvider/MainContextProvider'
import Card from '../src/components/Template/Card/Card'
import TableTemp from '../src/components/Template/Table/Table'
import ListTemp from '../src/components/Template/List/List'
import LineChart from '../src/components/General/Charts/LineChart'
import { MainColor } from '../src/utils/MainColor'
const {Text}=Typography;

const columns = [
  {
    title: 'NAME',
    dataIndex: 'name',
    key: 'name',
    render: text => <Text>{text}</Text>,
  },
  {
    title: 'STATUS',
    dataIndex: 'status',
    key: 'status',
    render: text => 
          {
            let color
            if (text === 'EXTENDED') {
              color = MainColor.Cyan;
            }
            if (text === 'TAX') {
                color = MainColor.Red;
              }
              if (text === 'SALE') {
                color = MainColor.Green;
              }
              return (
                <Tag color={color} key={text}>
                  {text.toUpperCase()}
                </Tag>
              );
  
          }
      
  },
  {
    title: 'DATE',
    dataIndex: 'date',
    key: 'date',
    render: text => <Text>{text}</Text>,
  },
  {
    title: 'PRICE',
    key: 'price',
    dataIndex: 'price',
    render: text => <Text>{text}</Text>,
  },
 
];

const dataSource = [
  {
    key: '1',
    name: 'DATA-1',
    status:'EXTENDED',
    date:'19-07-2015',
    price:'$68.2'
  },
  {
    key: '2',
    name: 'DATA-2',
    status:'TAX',
    date:'10-10-2015',
    price:'$75.7'
  },
  {
    key: '3',
    name: 'DATA-3',
    status:'TAX',
    date:'31-05-2016',
    price:'$102.2'
  },
  {
    key: '4',
    name: 'DATA-4',
    status:'SALE',
    date:'13-10-2016',
    price:'$30.3'
  },
  {
    key: '5',
    name: 'DATA-5',
    status:'TAX',
    date:'13-10-2016',
    price:'84.35'
  },
];
const index = () => (
  <div>
    <Head>
      <title>Dashboard</title>
    </Head>
    <MainContextProvider>
      <MainLayout>
        <Row>
          <Row type='flex' justify="space-around" align='top' gutter={[24, 24]}>
            <Col xs={24} md={12} lg={6}> <Card Title="Online Review" text='2781' icon="pay-circle" color='rgb(100, 234, 145)' /> </Col>
            <Col xs={24} md={12} lg={6}> <Card Title="New Customers" text='3241' icon="team" color='rgb(143, 201, 251)' />       </Col>
            <Col xs={24} md={12} lg={6}> <Card Title="Active Projects" text='253' icon="message" color='rgb(216, 151, 235)' />    </Col>
            <Col xs={24} md={12} lg={6}> <Card Title="Referrals" text='4324' icon="shopping-cart" color='rgb(246, 152, 153)' />  </Col>
          </Row>
          
          <Row type='flex' justify="start" gutter={[24, 24]}>
            <Col xs={24} md={14} lg={18}>
              <LineChart />
            </Col>
            <Col xs={24} md={10} lg={6}>
              <Col span={24}>
               <Row style={{background:MainColor.Cyan,width:'100%',height:215}}>

               </Row>
              </Col>
              <Col span={24}>
               <Row style={{background:MainColor.Purple,width:'100%',height:215}}>

               </Row>
              </Col>
            </Col>
           
          </Row>
       
          <Row type='flex' justify="space-around" gutter={[24, 24]}>
            <Col xs={23} md={11} style={{background:MainColor.White}}>
              <TableTemp HeaderList={columns}  DataSource={dataSource}/>
            </Col>
            <Col xs={23} md={12} style={{background:MainColor.White}}>
            <ListTemp  DataSource={dataSource}/>
           </Col>
           
          </Row>
       
        </Row>
      </MainLayout>
    </MainContextProvider>
  </div>
)

export default index
